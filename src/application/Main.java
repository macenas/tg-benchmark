package application;

import java.io.FileNotFoundException;
import java.io.PrintStream;

import config.BenchmarkParams;
import data.CSVReader;

public class Main {
	private static CSVReader input;
	private static MongoTests mongoTests;
	private static OracleTests oracleTests;
	
    public static void main(String[] args) {
    	// Reads input.
    	input = new CSVReader("input/empresas-atividades.csv");
    	
    	// Redirects standard output to file.
    	Main.redirectOutput();
    	
    	// Runs MongoDB benchmark.
    	Main.mongoTests = new MongoTests(input, BenchmarkParams.SUBSETS);
    	Main.mongoTests.start();

    	// Blank line between each DBMS tests set output.
    	System.out.println();
    	
    	// Runs Oracle XE 11g benchmark.
    	Main.oracleTests = new OracleTests(input, BenchmarkParams.SUBSETS);
    	Main.oracleTests.start();
    }
    
    private static void redirectOutput() {
    	
    	try {
			System.setOut(new PrintStream("out.txt"));
			System.setErr(new PrintStream("err.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    }
}