package application;

import i18n.PTBR;

import java.util.ArrayList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

import config.BenchmarkParams;
import config.MongoParams;
import data.CSVReader;
import data.MongoDB;

/***
 * MongoDB tests.
 */
public class MongoTests extends Tests {
	private MongoDB db;
	private DBCollection coll;
	
	public MongoTests(CSVReader input, int subsets) {
		super(input, subsets);
		
    	// Connects to MongoDB and selects the database and collection.
    	this.db = new MongoDB();
    	this.db.connect();
    	this.coll = db.getCollection(MongoParams.COLLECTION);
    	
    	// Clears collection.
    	this.clearCollection();
    }

	@Override
	protected void mainFlow() {
		this.mongoInsertLine();
		this.clearCollection();
		this.mongoInsertBatch();
		this.mongoSelectID();
		this.mongoSelectCNAE();
		this.mongoSelectRazao();
		this.mongoUpdateCNAE();
		this.mongoUpdateRazao();
		this.clearCollection();
	}

	@Override
	protected void finish() {
		this.db.disconnect();
	}
	
	private void clearCollection() {
		System.out.println(this.toString() + " " + PTBR.COLL_CLEANING);
		this.coll.remove(new BasicDBObject());
		System.out.println(this.toString() + " " + PTBR.COLL_CLEAN);
	}
	
	public void mongoInsertLine() {    	
		BasicDBObject obj;
    	int id = 0;
    	this.input.resetIterator();
    	this.startTimer("Inserção linha por linha");
    	
    	while(this.input.hasNext() && id < this.i) {
    		id++;
    		this.input.next();
    		obj = new BasicDBObject();
    		obj.append("_id", id);
    		obj.append("RAZAO_SOCIAL", this.input.getCurrent()[0]);
    		
    		try{
    			obj.append("CNAE", Integer.parseInt(this.input.getCurrent()[1]));
    		} catch (NumberFormatException e) {
    			obj.append("CNAE", -1);
    		}
    		
    		obj.append("DESCRICAO_ATIVIDADE", this.input.getCurrent()[2]);
    		obj.append("ATIVIDADE_PRINCIPAL", this.input.getCurrent()[3]);
    		obj.append("ATIV_PRINC_VIG_SANITARIA", this.input.getCurrent()[4]);
    		obj.append("ATIV_PRESTADORA_PRED", this.input.getCurrent()[5]);
    		coll.insert(obj);
    	}
    	    	
    	String time = this.stopTimer();
    	System.out.println(String.format(PTBR.ELEMENTS_INSERTED, i, time));
    }
	
	public void mongoInsertBatch() {    	
		System.out.println(this.toString() + " " + PTBR.INSERT_PREP);
    	BasicDBObject[] documents = new BasicDBObject[this.i];
		BasicDBObject obj;
    	int id = 0;
    	this.input.resetIterator();
    	
    	while(this.input.hasNext() && id < this.i) {
    		id++;
    		this.input.next();
    		obj = new BasicDBObject();
    		obj.append("_id", id);
    		obj.append("RAZAO_SOCIAL", this.input.getCurrent()[0]);
    		
    		try{
    			obj.append("CNAE", Integer.parseInt(this.input.getCurrent()[1]));
    		} catch (NumberFormatException e) {
    			obj.append("CNAE", -1);
    		}
    		
    		obj.append("DESCRICAO_ATIVIDADE", this.input.getCurrent()[2]);
    		obj.append("ATIVIDADE_PRINCIPAL", this.input.getCurrent()[3]);
    		obj.append("ATIV_PRINC_VIG_SANITARIA", this.input.getCurrent()[4]);
    		obj.append("ATIV_PRESTADORA_PRED", this.input.getCurrent()[5]);
    		documents[id - 1] = obj;
    	}
    	    	
    	this.startTimer("Inserção em batch");
    	coll.insert(documents);
    	String time = this.stopTimer();
    	System.out.println(String.format(PTBR.ELEMENTS_INSERTED, i, time));
    }
    
	private void mongoSelectID() {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		
		for (String id: BenchmarkParams.IDS.split(", "))
			ids.add(Integer.parseInt(id));
		
		BasicDBObject objWhere = new BasicDBObject("_id", new BasicDBObject("$in", ids));
		this.startTimer(PTBR.SELECT_ID_TEST);
		
		for (int i = 0; i < BenchmarkParams.ITERATIONS; i++)
			 this.coll.find(objWhere);
		
		String time = this.stopTimer();
		System.out.println(
				String.format(PTBR.SEL_UPDT_ITERATIONS, BenchmarkParams.ITERATIONS, time));		
	}
	
	private void mongoSelectCNAE() {
		ArrayList<Integer> cnaes = new ArrayList<Integer>();
		
		for (String cnae: BenchmarkParams.CNAES.split(", "))
			cnaes.add(Integer.parseInt(cnae));
		
		BasicDBObject objWhere = new BasicDBObject("CNAE", new BasicDBObject("$in", cnaes));
		this.startTimer(PTBR.SELECT_CNAE_TEST);
		
		for (int i = 0; i < BenchmarkParams.ITERATIONS; i++)
			 this.coll.find(objWhere);
		
		String time = this.stopTimer();
		System.out.println(
				String.format(PTBR.SEL_UPDT_ITERATIONS, BenchmarkParams.ITERATIONS, time));	
	}
	
	private void mongoSelectRazao() {
		BasicDBObject objWhere =
				new BasicDBObject("RAZAO_SOCIAL", new BasicDBObject("$regex", ".*LEITE.*"));
		this.startTimer(PTBR.SELECT_RS_TEST);
		
		for (int i = 0; i < BenchmarkParams.ITERATIONS; i++)
			 this.coll.find(objWhere);
		
		String time = this.stopTimer();
		System.out.println(
				String.format(PTBR.SEL_UPDT_ITERATIONS, BenchmarkParams.ITERATIONS, time));
	}
	
	private void mongoUpdateCNAE() {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		
		for (String id: BenchmarkParams.IDS.split(", "))
			ids.add(Integer.parseInt(id));
		
		BasicDBObject objSelect = new BasicDBObject("_id", new BasicDBObject("$in", ids));
		BasicDBObject objUpdate = new BasicDBObject("$set", (new BasicDBObject("CNAE", 9999999)));
		this.startTimer(PTBR.UPDATE_CNAE_TEST);
		
		for (int i = 0; i < BenchmarkParams.ITERATIONS; i++)
			 this.coll.update(objSelect, objUpdate, false, true);
		
		String time = this.stopTimer();
		System.out.println(
				String.format(PTBR.SEL_UPDT_ITERATIONS, BenchmarkParams.ITERATIONS, time));
	}
	
	private void mongoUpdateRazao() {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		
		for (String id: BenchmarkParams.IDS.split(", "))
			ids.add(Integer.parseInt(id));
		
		BasicDBObject objSelect = new BasicDBObject("CNAE", 9999999);
		BasicDBObject objUpdate = 
				new BasicDBObject("$set", (new BasicDBObject("RAZAO_SOCIAL", "TESTE")));
		this.startTimer(PTBR.UPDATE_RS_TEST);
		
		for (int i = 0; i < BenchmarkParams.ITERATIONS; i++)
			 this.coll.update(objSelect, objUpdate, false, true);
		
		String time = this.stopTimer();
		System.out.println(
				String.format(PTBR.SEL_UPDT_ITERATIONS, BenchmarkParams.ITERATIONS, time));	
	}
	
	@Override
	public String toString() {
		return "[MongoDB Tests]";
	}
}