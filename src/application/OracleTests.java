package application;

import i18n.PTBR;
import config.BenchmarkParams;
import config.OracleParams;
import data.CSVReader;
import data.OracleDB;

/***
 * Oracle tests.
 */
public class OracleTests extends Tests {
	private OracleDB db;
	
	public OracleTests(CSVReader input, int subsets) {
		super(input, subsets);
		
    	// Connects to Oracle.
    	this.db = new OracleDB();
    	this.db.connect();
    	
    	// Prepare table to get data. If it already exists, drops it and recreates.
    	String columns = "id number(6) primary key,"
    			+ "RAZAO_SOCIAL varchar2(60),"
    			+ "CNAE number(7),"
    			+ "DESCRICAO_ATIVIDADE varchar2(100),"
    			+ "ATIVIDADE_PRINCIPAL varchar2(90),"
    			+ "ATIV_PRINC_VIG_SANITARIA varchar2(80),"
    			+ "ATIV_PRESTADORA_PRED varchar2(60)";
    	this.db.createOrReplaceTable(OracleParams.TABLE, columns);
    }

	@Override
	protected void mainFlow() {
		this.oracleInsertLine();
		this.clearTable();
		this.oracleInsertBatch();
		this.oracleSelectID();
		this.oracleSelectCNAE();
		this.oracleSelectRazao();
		this.oracleUpdateCNAE();
		this.oracleUpdateRazao();		
		this.clearTable();
	}

	@Override
	protected void finish() {
		this.db.disconnect();
	}
	
	private void clearTable() {
		System.out.println(this.toString() + " " + PTBR.TABLE_CLEANING);
		this.db.executeUpdate(String.format("delete from \"%s\"", OracleParams.TABLE));
		System.out.println(this.toString() + " " + PTBR.TABLE_CLEAN);
	}
	
	private void oracleInsertLine() {
		int id = 0;
    	String SQLStmt = String.format(
    			"insert into \"%s\" values (?, ?, ?, ?, ?, ?, ?)", OracleParams.TABLE);
    	this.db.prepareStatement(SQLStmt);
    	this.input.resetIterator();
    	this.startTimer(PTBR.INSERTION_TEST);
    	
    	while(this.input.hasNext() && id < this.i) {
    		id++;
    		this.input.next();
			this.db.setInt(1, id);
			this.db.setString(2, this.input.getCurrent()[0]);
			
			try {
				this.db.setInt(3, Integer.parseInt(this.input.getCurrent()[1]));
			} catch (NumberFormatException e) {
				this.db.setInt(3, -1);
			}
			
			this.db.setString(4, this.input.getCurrent()[2]);
			this.db.setString(5, this.input.getCurrent()[3]);
			this.db.setString(6, this.input.getCurrent()[4]);
			this.db.setString(7, this.input.getCurrent()[5]);
			this.db.preparedUpdate();
    	}

    	this.db.commit();
    	String time = this.stopTimer();
    	System.out.println(String.format(PTBR.ELEMENTS_INSERTED, this.i, time));
	}
	
	private void oracleInsertBatch() {
		System.out.println(this.toString() + " " + PTBR.INSERT_PREP);
		int id = 0;
    	String SQLStmt = String.format(
    			"insert into \"%s\" values (?, ?, ?, ?, ?, ?, ?)", OracleParams.TABLE);
    	this.db.prepareStatement(SQLStmt);
    	this.input.resetIterator();
    	
    	while(this.input.hasNext() && id < this.i) {
    		id++;
    		this.input.next();
			this.db.setInt(1, id);
			this.db.setString(2, this.input.getCurrent()[0]);
			
			try {
				this.db.setInt(3, Integer.parseInt(this.input.getCurrent()[1]));
			} catch (NumberFormatException e) {
				this.db.setInt(3, -1);
			}
			
			this.db.setString(4, this.input.getCurrent()[2]);
			this.db.setString(5, this.input.getCurrent()[3]);
			this.db.setString(6, this.input.getCurrent()[4]);
			this.db.setString(7, this.input.getCurrent()[5]);
			this.db.addBatch();
    	}

    	this.startTimer(PTBR.INSERTION_TEST);
    	this.db.executeBatch();
    	this.db.commit();
    	String time = this.stopTimer();
    	System.out.println(String.format(PTBR.ELEMENTS_INSERTED, this.i, time));
	}
	
	public void oracleSelectID() {
		String SQLStmt = "select * from \"%s\" where ID in (%s)";
		this.startTimer(PTBR.SELECT_ID_TEST);
		
		for (int i = 0; i < BenchmarkParams.ITERATIONS; i++)
			this.db.executeQuery(String.format(SQLStmt, OracleParams.TABLE, BenchmarkParams.IDS));
		
		String time = this.stopTimer();
		System.out.println(
				String.format(PTBR.SEL_UPDT_ITERATIONS, BenchmarkParams.ITERATIONS, time));
	}
	
	private void oracleSelectCNAE() {
		String SQLStmt = "select * from \"%s\" where CNAE in (%s)";
		this.startTimer(PTBR.SELECT_CNAE_TEST);
		
		for (int i = 0; i < BenchmarkParams.ITERATIONS; i++)
			this.db.executeQuery(String.format(SQLStmt, OracleParams.TABLE, BenchmarkParams.CNAES));
		
		String time = this.stopTimer();
		System.out.println(
				String.format(PTBR.SEL_UPDT_ITERATIONS, BenchmarkParams.ITERATIONS, time));
	}
	
	private void oracleSelectRazao() {
		String SQLStmt = "select * from \"%s\" where RAZAO_SOCIAL like '%s'";
		this.startTimer(PTBR.SELECT_RS_TEST);
		
		for (int i = 0; i < BenchmarkParams.ITERATIONS; i++)
			this.db.executeQuery(String.format(
					SQLStmt, OracleParams.TABLE, BenchmarkParams.RAZAO_SOCIAL));
		
		String time = this.stopTimer();
		System.out.println(
				String.format(PTBR.SEL_UPDT_ITERATIONS, BenchmarkParams.ITERATIONS, time));
	}

	private void oracleUpdateCNAE() {
		String SQLStmt = "update \"%s\" set CNAE = 9999999 where ID in (%s)";
		this.startTimer(PTBR.UPDATE_CNAE_TEST);
		
		for (int i = 0; i < BenchmarkParams.ITERATIONS; i++)
			this.db.executeUpdate(String.format(SQLStmt, OracleParams.TABLE, BenchmarkParams.IDS));
		
		this.db.commit();
		String time = this.stopTimer();
		System.out.println(
				String.format(PTBR.SEL_UPDT_ITERATIONS, BenchmarkParams.ITERATIONS, time));
	}
	
	private void oracleUpdateRazao() {
		String SQLStmt = "update \"%s\" set RAZAO_SOCIAL = 'TESTE' where CNAE = 9999999";
		this.startTimer(PTBR.UPDATE_RS_TEST);
		
		for (int i = 0; i < BenchmarkParams.ITERATIONS; i++)
			this.db.executeUpdate(String.format(SQLStmt, OracleParams.TABLE));
		
		this.db.commit();
		String time = this.stopTimer();
		System.out.println(
				String.format(PTBR.SEL_UPDT_ITERATIONS, BenchmarkParams.ITERATIONS, time));
	}
	
	@Override
	public String toString() {
		return "[Oracle Tests]";
	}
}