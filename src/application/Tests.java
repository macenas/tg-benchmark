package application;

import i18n.PTBR;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

import data.CSVReader;

/**
 * Abstract tests set.
 * 
 * @author Marcelo Nascimento Oliveira.
 *
 */
public abstract class Tests {
	protected CSVReader input;
	protected int i;
	protected int increment;
	private long time;
	
	protected abstract void mainFlow();

	public Tests(CSVReader input, int subsets) {
		this.input = input;
		this.increment = (this.input.getCount() / subsets);
	}
	
	public void start() {
		// Prints a test set running begin message.
		System.out.println(this.toString() + " " + PTBR.INI_MSG);

		// Runs the set of tests.
		for (i = this.increment; i <= this.input.getCount(); i += this.increment) {
			this.mainFlow();
			System.out.println();
		}
		
		// Executes final tasks like closing connections.
		this.finish();
		
		// Prints a test set running end message.
		System.out.println(this.toString() + " " + PTBR.END_MSG);
	}
	
	protected abstract void finish();
	
	protected void startTimer(String test) {
		System.out.print(this.toString() + " " + test + ": ");
		this.time = getCpuTime();
	}
	
	protected String stopTimer() {
		this.time = (this.getCpuTime() - this.time);
		this.time = this.time / 1000000; // Converting to miliseconds.
		int min = (int) (this.time / 60000);
		int seg = (int) ((this.time / 1000) % 60);
		int ms  = (int) (this.time % 1000);
		return (min + ":" + seg + "." + ms + "min");
	}
	
	/** Get CPU time in nanoseconds. */
	private long getCpuTime() {
	    ThreadMXBean bean = ManagementFactory.getThreadMXBean( );
	    return bean.isCurrentThreadCpuTimeSupported( ) ?
	        bean.getCurrentThreadCpuTime( ) : 0L;
	}
	 
	/** Get user time in nanoseconds. */
//	private long getUserTime() {
//	    ThreadMXBean bean = ManagementFactory.getThreadMXBean( );
//	    return bean.isCurrentThreadCpuTimeSupported( ) ?
//	        bean.getCurrentThreadUserTime( ) : 0L;
//	}
//
	/** Get system time in nanoseconds. */
//	private long getSystemTime() {
//	    ThreadMXBean bean = ManagementFactory.getThreadMXBean( );
//	    return bean.isCurrentThreadCpuTimeSupported( ) ?
//	        (bean.getCurrentThreadCpuTime( ) - bean.getCurrentThreadUserTime( )) : 0L;
//	}
}