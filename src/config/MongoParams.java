package config;

public class MongoParams {
	public static final String ADDRESS = "localhost";
	public static final String DB = "benchmark";
	public static final String COLLECTION = "test";
}
