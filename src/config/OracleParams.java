package config;

public class OracleParams {
	public static final String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	public static final String DB = "benchmark";
	public static final String USER = "SYSTEM";
	public static final String PASS = "123456";
	public static final String TABLE = "test";
}
