package data;

import i18n.PTBR;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class CSVReader {
	String[] keys;
	ArrayList<String[]> values;
	Iterator<String[]> it;
	String[] curr;
	
	public CSVReader(String filename) {
		InputFile input;
		values = new ArrayList<String[]>();
		
		try {
			input = new InputFile(filename);
			this.keys = input.read().split(";");
				
			String[] splitLine;
			String line = input.read();
			
			while (line != null) {
				splitLine = line.split(";");
				this.values.add(splitLine);
				line = input.read();
			}
			
		} catch (IOException e) {
			System.err.println(this.toString() + PTBR.INPUT_ERROR + " " + e.getMessage());
			System.exit(-1);
		}
	}
	
	public String[] getElement(int index) {
		return this.values.get(index);
	}
	
	public int getCount() {
		return this.values.size();
	}
	
	public void resetIterator() {
		this.it = this.values.iterator();
	}
	
	public boolean hasNext() {
		return this.it.hasNext();
	}
	
	public void next() {
		this.curr = this.it.next();
	}
	
	public String[] getCurrent() {
		return this.curr;
	}
	
	@Override
	public String toString() {
		return "[CSVReader]";
	}
}
