package data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class InputFile {
	private FileReader reader;
	private BufferedReader buffer;
	
	public InputFile(String filename) throws IOException {
	    reader = new FileReader(filename);
	    buffer = new BufferedReader(reader);
	}
	
	public String read() throws IOException {
		return this.buffer.readLine();
	}
	
	public void close() throws IOException {
		this.reader.close();
		this.buffer.close();
	}
}