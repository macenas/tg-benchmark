package data;

import i18n.PTBR;

import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

import config.MongoParams;

public class MongoDB {
    private MongoClient client;
    private DB db;
    
    public MongoDB() {
    	this.client = null;
    }

    public void connect() {

		if (this.client == null) {
			
			try {
				this.client = new MongoClient(MongoParams.ADDRESS);
				System.out.println(this.toString() + " " + PTBR.CONNECTED);
			} catch (UnknownHostException e) {
				System.err.println(this.toString() + PTBR.CONNECTION_ERROR + " " + e.getMessage());
				System.exit(-1);
			}
		}

        this.db = client.getDB(MongoParams.DB);
        System.out.println(this.toString() + " " + PTBR.DB_SELECTED + MongoParams.DB);
    }
	
    public void disconnect() {
    	this.client.close();
    }
    
	public DBCollection getCollection(String collName) {
		System.out.println(this.toString() + " " + PTBR.COLL_SELECTED + MongoParams.COLLECTION);
		return this.db.getCollection(collName);
	}
	
	@Override
	public String toString() {
		return "[MongoDB]";
	}
}