package data;

import i18n.PTBR;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import config.OracleParams;


public class OracleDB {
	Connection conn = null;
	ResultSet queryResult = null;
	PreparedStatement prepStmt;
	Statement stmt;
	
	public OracleDB() {
		
		try {
			DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
		} catch (SQLException e) {
			System.err.println(PTBR.CONNECTION_ERROR + " " + e.getMessage());
			System.exit(-1);
		}
	}
	
	public void connect() {
		
		try {
			this.conn = DriverManager.getConnection
					(OracleParams.URL, OracleParams.USER, OracleParams.PASS);
			this.conn.setAutoCommit(false);
			System.out.println(this.toString() + " " + PTBR.CONNECTED);
		} catch (SQLException e) {
			System.err.println(PTBR.CONNECTION_ERROR + " " + e.getMessage());
			System.exit(-1);
		}
	}
		
	public void disconnect() {
		
		try {
			this.conn.close();
		} catch (SQLException e) {
			System.err.println(PTBR.DISCONNECTION_ERROR + " " + e.getMessage());
			System.exit(-1);
		}
	}
	
	public void createOrReplaceTable(String tableName, String columns) {
		String alreadyExistsMsg = this.toString() + " " + PTBR.TABLE_ALREADY_EXISTS;
		String createdMsg = this.toString() + " " + PTBR.TABLE_CREATED;
		String existsCheckQuery = "select count(*) from all_tables where table_name like '%s'";
    	
		// Checks if table already exists.
		this.executeQuery(String.format(existsCheckQuery, tableName));
    	this.next();
    	    	
    	// Drop if exists.
    	if (this.getInt(1) != 0) {
    		this.executeUpdate(String.format("drop table \"%s\"", tableName));
    		System.out.println(String.format(alreadyExistsMsg, tableName));
    	}

    	// Recreate.
    	this.executeUpdate(String.format("create table \"%s\" (%s)", tableName, columns));
    	this.commit();
    	System.out.println(String.format(createdMsg, tableName));
	}
	
	public boolean next() {
		boolean result = false;
		
		if (this.queryResult == null)
			System.err.println(PTBR.NULL_CURSOR);
		
		try {
			result = this.queryResult.next();
		} catch (SQLException e) {
			System.err.println(this.toString() + " " + PTBR.QUERY_ERROR);
			e.printStackTrace();
			System.exit(-1);
		}
		
		return result;
	}
	
	public int getInt(int columnIndex) {
		int result = -1;
		
		try {
			result = this.queryResult.getInt(columnIndex);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			System.exit(-1);
		}
		
		return result;
	}
	
	public String getString(int columnIndex) {
		String result = null;
		
		try {
			result = this.queryResult.getString(columnIndex);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			System.exit(-1);
		}
		
		return result;
	}
	
	public void commit() {
		
		try {
			this.conn.commit();
		} catch (SQLException e) {
			System.err.println(String.format(PTBR.COMMIT_ERROR));
			System.err.println(e.getMessage());
			System.exit(-1);
		}
	}
	
	public void executeQuery(String SQLStmt) {
		
		try {
			
			if (this.queryResult != null && !queryResult.isClosed())
				this.queryResult.close();
			
			if (this.stmt != null && !stmt.isClosed())
				this.stmt.close();
			
			this.stmt = this.conn.createStatement();
			this.queryResult = stmt.executeQuery(SQLStmt);
		} catch (SQLException e) {
			System.err.println(String.format(PTBR.QUERY_ERROR, SQLStmt));
			System.err.println(e.getMessage());
			System.exit(-1);
		}
	}
	
	public int executeUpdate(String SQLStmt) {
		int result = -1;
		
		try {
			
			if (this.stmt != null && !stmt.isClosed())
				this.stmt.close();
			
			this.stmt = this.conn.createStatement();
			result = stmt.executeUpdate(SQLStmt);
		} catch (SQLException e) {
			System.err.println(String.format(PTBR.QUERY_ERROR, SQLStmt));
			System.err.println(e.getMessage());
			System.exit(-1);
		}
		
		return result;
	}
	
	public void prepareStatement(String SQLStmt) {
		
		try {
			
			if (this.prepStmt != null && !prepStmt.isClosed())
				this.prepStmt.close();
			
			this.prepStmt = this.conn.prepareStatement(SQLStmt);
		} catch (SQLException e) {
			System.err.println(String.format(PTBR.QUERY_ERROR, SQLStmt));
			System.err.println(e.getMessage());
			System.exit(-1);
		}
	}
	
	public void setInt(int index, int value) {
		
		try {
			this.prepStmt.setInt(index, value);
		} catch (SQLException e) {
			System.err.println(String.format(PTBR.PREP_STMT_BIND_ERROR, value, index));
			System.err.println(e.getMessage());
			System.exit(-1);
		}
	}
	
	public void setString(int index, String value) {
		
		try {
			this.prepStmt.setString(index, value);
		} catch (SQLException e) {
			String param = ("\"" + value + "\"");
			System.err.println(String.format(PTBR.PREP_STMT_BIND_ERROR, param, index));
			System.err.println(e.getMessage());
			System.exit(-1);
		}
	}
	
	public void addBatch() {
	
		try {
			this.prepStmt.addBatch();
		} catch (SQLException e) {
			System.err.println(String.format(PTBR.PREP_STMT_ERROR));
			System.err.println(e.getMessage());
			System.exit(-1);
		}
	}
	
	public void executeBatch() {
		
		try {
			this.prepStmt.executeBatch();
		} catch (SQLException e) {
			System.err.println(String.format(PTBR.PREP_STMT_ERROR));
			System.err.println(e.getMessage());
			System.exit(-1);
		}
	}
	
	public void preparedQuery() {
		
		try {
			
			if (this.queryResult != null && !queryResult.isClosed())
				this.queryResult.close();
			
			this.queryResult = this.prepStmt.executeQuery();
		} catch (SQLException e) {
			System.err.println(PTBR.PREP_STMT_ERROR);
			System.err.println(e.getMessage());
			System.exit(-1);
		}
	}
	
	public int preparedUpdate() {
		int result = -1;
		
		try {
			result = this.prepStmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(String.format(PTBR.PREP_STMT_ERROR));
			System.err.println(e.getMessage());
			System.exit(-1);
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		return "[Oracle]";
	}
}