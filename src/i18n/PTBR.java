package i18n;

public class PTBR {
	// *** ERRORS *** //
	
	// Connection
    public static final String CONNECTION_ERROR = "Erro de conexão.";
    public static final String DISCONNECTION_ERROR = "Erro ao encerrar conexão.";
    
    // SQL
    public static final String QUERY_ERROR = "Erro ao executar consulta SQL: \"%s\"";
    public static final String COMMIT_ERROR = "Erro ao persistir alterações.";
    public static final String PREP_STMT_ERROR = "Erro ao executar prepared statement.";
    public static final String PREP_STMT_BIND_ERROR
    		= "Erro ao associar valor %s ao parâmetro %d do prepared statement.";
    public static final String NULL_CURSOR = "Não há cursor aberto para iteração.";
    
    // CSV reader
    public static final String INPUT_ERROR = "Erro na entrada.";

    // Benchmark
    public static final String TEST_EXEC_ERROR = "Erro ao executar teste: ";
    
    // *** INFORMATION *** //
    
    // Benchmark
    public static final String INI_MSG = "*** Início dos testes ***";
    public static final String END_MSG = "*** Fim dos testes ***";
    public static final String INSERTION_TEST = "Insert";
    public static final String INSERT_PREP = "Preparando batch de inserção.";
    public static final String ELEMENTS_INSERTED = "%s elementos inseridos em %s.";
    public static final String SELECT_ID_TEST = "Select ID";
    public static final String SELECT_CNAE_TEST = "Select CNAE";
    public static final String SELECT_RS_TEST = "Select RAZAO_SOCIAL";
    public static final String UPDATE_CNAE_TEST = "Update CNAE";
    public static final String UPDATE_RS_TEST = "Update RAZAO_SOCIAL";
    public static final String SEL_UPDT_ITERATIONS = "%d iterações realizadas em %s.";
    
    // Database
    public static final String CONNECTED = "Conectado.";
    public static final String DB_SELECTED = "Banco de dados selecionado: ";

    // Database >> MongoDB
    public static final String COLL_SELECTED = "Coleção selecionada: ";
    public static final String COLL_CLEANING = "Removendo todos os documentos da coleção atual.";
    public static final String COLL_CLEAN = "Coleção vazia.";
    
    // Database >> Oracle
    public static final String TABLE_ALREADY_EXISTS = "Tabela \"%s\" já existente. Excluindo...";
    public static final String TABLE_CREATED = "Tabela \"%s\" criada.";
    public static final String TABLE_CLEANING = "Removendo todas as linhas da tabela atual.";
    public static final String TABLE_CLEAN = "Tabela vazia.";
}
